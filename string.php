<?php

#Hierarquia das funções: NO php é feito em ordem de cadeia de dentro pra fora  (<=(<=(dentro)))

$nome = 'Paola Biscaro';
$cpf = '222.222.222-55';
$cpfTratado = substr($cpf, 0, 11);    //"substr" faz o recorte da string (remove um determinado pedaço da string, mantem apenas o indicado no código)


#Como pegar pedaços de uma String...

echo $nome[0];  //Pega uma posição de um elemento da string
echo"<hr>";
echo $cpf;    
echo"<hr>";
echo $cpfTratado;
echo"<hr>";

echo str_replace(".", "", $cpfTratado);     //Remove ou altera caracteres de uma string (1° O que quero substituir, 2° O que vou colocar)
echo"<hr>";

echo str_replace(".", "", substr($cpf, 0, 11));


#Como tratar datas...

echo"<hr>";

$data = '20/05/2012' ;
$dataArray = explode('/', $data);

var_dump($dataArray);


