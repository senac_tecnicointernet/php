<?php


$cadastro = [

    [
        'nome' => 'José Carlos' ,
        'cpf' => '222.222.333-77',
        'data_nasc' => '1972-05-20',
        'cidade' => 'Marília',
    ],
    [
        'nome' => 'Roseane da Silva' ,
        'cpf' => '225.365.258-88',
        'data_nasc' => '1983-01-25',
        'cidade' => 'Oriente',
    ],
    [
        'nome' => 'Marcos dos Santos' ,
        'cpf' => '258.369.147-78',
        'data_nasc' => '1963-07-03',
        'cidade' => 'Marília',
    ],
];

echo "Nome: ", $cadastro[0]['nome'];
echo "<br>";
echo "CPF: ", $cadastro [0]['cpf'];
echo "<br>";
echo "Data de Nascimento: ",  $cadastro[0]['data_nasc'];
echo "<br>";
echo "Cidade: ", $cadastro[0]['cidade'];

echo "<hr>";

echo "Nome: ", $cadastro[1]['nome'];
echo "<br>";
echo "CPF: ", $cadastro [1]['cpf'];
echo "<br>";
echo "Data de Nascimento: ",  $cadastro[1]['data_nasc'];
echo "<br>";
echo "Cidade: ", $cadastro[1]['cidade'];

echo "<hr>";

echo "Nome: ", $cadastro[2]['nome'];
echo "<br>";
echo "CPF: ", $cadastro [2]['cpf'];
echo "<br>";
echo "Data de Nascimento: ",  $cadastro[2]['data_nasc'];
echo "<br>";
echo "Cidade: ", $cadastro[2]['cidade'];

echo "<hr>";
