<?php

$nome = "Paola Biscaro";

echo $nome;

$idade = 18;

echo"<br>";

echo($idade);

//Comentario
#comentario 

/*
comentario em blocos 
...
*/

echo"<hr>";

$salario = 10000.50;

echo $salario;

echo"<hr>";

echo "R$ " . number_format($salario, 2, "," ,".");

echo"<hr>";

$nome = "Paola";
$sobrenome = "Biscaro";

echo $nome. " ". $sobrenome;

echo"<hr>";

echo "Meu nome é: $nome {$sobrenome}";  //outro jeito de fazer string, usandos aspas duplas "", tambem da pra usar em outros programas as {} para diferenciar variaveis do texto.

echo"<br>";
echo 'Meu nome é: $nome $sobrenome';    //usando aspas simples '' , sua execução é mais rapida tambem, por isso ela é usada!!

echo"<hr>";

echo 'A caixa d\'água esta vazia';      // a barra invertida é um escape dos caracteres iguais, é como se ele pulasse pro próximo, (melhor usar as aspas "")

echo"<hr>";

//Aula de "PHP"

echo 'Aula de "PHP"';
echo "<br>";
echo "Aula de \"PHP\"";





?>