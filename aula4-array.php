<?php

$listaCompra = ['Arroz', 'Feijão', 'Banana', 'Detergente', 'Sabonete'];

$listaCompra[] = 'Ração PET';       //adicionar itens no array


echo "<pre>";
print_r($listaCompra);
echo "<pre>";

// echo"<pre>"; Apenas melhora a visualização de conteudo, é preciso para o print_r


echo "<hr>";

var_dump($listaCompra);

echo"<hr>";


//echo $listaCompra [4];
echo $listaCompra [4]. ", ".$listaCompra[3];

echo "<hr>";

//foreach ($variable as $key => $value){};



#Array Unidimensional

foreach ($listaCompra as $item) {    //Imprime todas as informações
    echo $item;
    echo "<br>";
};

echo "<hr>";



#Array Associativo

$funcionario = [
    'nome' => 'Paola Biscaro',      //'nome' é o index, é a associassão das vaariaveis a uma descrição
    'cargo' => 'Estudante',
    'idade' => 18,
    'salario' => 1200.50,
    'status' => true
];

var_dump($funcionario);
echo $funcionario ['cargo'];

echo "<hr>";


#Array Multidimensional

$funcionarios = [
    [
        'nome' => 'Paola Biscaro',
        'cargo' => 'Estudante',
        'idade' => 18,
        'salario' => 1200.50,
        'status' => true,
        'cursos' => ['web', 'php', 'Javascript']
    ],

    [
        'nome' => 'João',
        'cargo' => 'MEP',
        'idade' => 45,
        'salario' => 2000.00,
        'status' => false, 
        'cursos' => []
    ],

    [
        'nome' => 'Lurdinha',
        'cargo' => 'MEP',
        'idade' => 50,
        'salario' => 1500.00,
        'status' => true, 
        'cursos' => ['Photoshop', 'Illustrator']

    ]

];

var_dump($funcionarios);
echo $funcionarios[2]['cursos'][1];

echo "<hr>";

echo "Nome: ", $funcionarios[2]['nome'];
echo "<br>";
echo "Cargo: ", $funcionarios[2]['cargo'];
echo "<br>";
echo "Curso: ", $funcionarios[2]['cursos'][0];

echo "<hr>";





foreach($funcionarios as $item){
    echo "Nome: ". $item['nome'];
    echo "<br>";
    echo "Cargo: ". $item['cargo'];
    echo "<br>";
    echo "Idade: ". $item['idade'];
    echo "<br>";
    echo "Cursos: ".implode(", ", $item['cursos']);
    echo "<hr>";

}