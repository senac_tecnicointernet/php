<?php
    $nomeAluno = 'Paola Pereira';
    $curso = 'PHP';
    $frequencia = '76%';
    $nota = 10;


?>


<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Informações do Aluno</title>
</head>
<body>
        <h1>Dados do Aluno</h1>
        
        <p>
            <strong>Nome:</strong> <?php echo $nomeAluno; ?> 
        </p>
        <p>
            <strong>Curso:</strong> <?php echo $curso; ?>
        </p>
        <p>
            <strong>Frequência:</strong> <?= $frequencia; ?>
        </p>
        <p>
            <strong>Nota:</strong> <?= $nota; ?>
        </p>

        <p>
            A aluna <?= $nomeAluno; ?> , frequentou o curso de <?= $curso; ?>  com <?=$frequencia; ?> de presença.
        </p>
    
    
</body>
</html>


