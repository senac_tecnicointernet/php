<?php

$nome = 'Paola Biscaro';
$idade = 18;
$email = 'paolabisc@gmail.com';
$senha = '123456789';
$cursos = ['PHP', 'HTML', 'CSS'];

echo "<h1> Trabalhando com Estrutura Condicional </h1>";    

// Verificar se um usuário é menor ou maior de idade APENAS VERDADEIRO
echo "<h2>Exemplo de IF (se...)</h2>";
    if($idade >= 18)
    {
        echo "O usuário $nome é maior de idade";
    }

echo '<hr>';

//IF Ternario (uma linha só), tem apenas um retorno
echo "<h2>Exemplo de IF  Ternário (se...)</h2>";
    echo ($idade >= 18) ? 'Maior de idade' : 'Menor de idade';

    echo '<hr>';


// Verifica a validação de email e senha de forma DIRETA
echo "<h2>Exemplo de IF e ELSE (se e senão)</h2>";

    if($email ==  'paolabisc@gmail.com' && $senha == '123456789')
    {
        echo 'Usuario Logado!';
    }else{
        echo "Usuário ou Senha inválido";
    }
echo '<br>';

// Verifica a validação de email e senha com 2 IF
    if( $senha == '123456789')
    {
        if($email ==  'paolabisc@gmail.com' )
        {
            echo 'Usuario Logado!';
        }else{
            echo "Usuário ou Senha inválido";
        }
    }else{
        echo "Usuário ou Senha inválido";
    }


    echo '<hr>';

//Condição Encadeada
echo "<h2>Exemplo de Multiplas Condições</h2>";

$num1 = 30;
$num2 = 10;

    if($num1 == $num2)
    {
        echo 'Os números são iguais';

    } else if($num1 > $num2){
        echo "$num1 é maior que $num2";

    }else {
        echo "$num2 é maior que  $num1";
    }

    echo '<hr>';



//Get é tudo que tem como parametros na barra de URL (Menos seguro)
echo "<h2>Exemplo de GET</h2>";


$menu = $_GET['menu'] ?? "home";   #atribui tudo que esta na url ao array, dando uma segunda opção

switch(strtolower($menu)){      #Direciona o usuário para varias partes do site / strtolower converte todas as letras para minusculas
    case "home":
        echo 'Pagina Principal';
        break;
    case "empresa";
        echo 'Pagina Empresas';
        break;
    case "produtos";
        echo 'Pagina Produtos';
        break;
    case "contato";
         echo 'Pagina Contato';
        break;
    default:
        echo 'Pagina Erro 404';
}
#echo $_GET ['menu'];    //_GET é um Array
#var_dump($_GET);