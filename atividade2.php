<?php

$nome = 'Paola Biscaro';
$nacionalidade = 'brasileira';
$cargo = 'docente';
$cpf = '111.111.111-11';
$rg = '22.222.222-2';
$empresa = 'Senac';
$cnpj = '52.757.388/0001-00';
$endereco = 'Rua Paraiba, 125';
$cidade = 'Marilia-SP';
$cep = '17570-000';

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Declaração de Local de Trabalho</title>
</head>
<body>

    <h1>DECLARAÇÃO DE LOCAL DE TRABALHO</h1> 

    <P>
        
        <p> Eu, <?= $nome ?>, <?=$nacionalidade?>, <?=$cargo?>, inscrito(a) no CPF sob o nº <?=$cpf ?> e no RG nº  <?=$rg ?> , declaro para os devidos fins que possuo vínculo empregatício com a empresa  <?=$empresa ?> inscrita no CNPJ sob o nº  <?=$cnpj ?>, localizada à  <?=$endereco?> <?=$cidade?>,  <?=$cep ?>. </p>
       

        <p>Por ser expressão da verdade, firmo a presente para efeitos legais. </p>
        

        <p><?=$cidade?>, 15 de Setembro de 2022 </p>
      

        <?= $nome ?>
  

    </P>
    
</body>
</html>