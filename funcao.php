<?php


 //Cria funções, seria como um algoritmo chave para ser usado, para tratar dados, podem ou não resultar um valor(void)


 function soma($num1, $num2)
 {
     $total = $num1+ $num2;
     return $total;
 };



 function multiplicacao($n1, $n2)
 {
     $total = $n1 * $n2;
     return $total;
 };



 function divisao($n1, $n2)
 {
     $total = $n1 / $n2;
     return $total;
 };



 function subtracao($n1, $n2)
 {
     $total = $n1 - $n2;
     return $total;
 }



 echo soma(10, 5);
 echo "<hr>";
 echo multiplicacao(29, 4);
 echo"<hr>";
 echo divisao(2, 11);
 echo"<hr>";
 echo subtracao(2, 5);
 echo "<hr>";



 // Function que retorna par ou impar
 function par_ou_impar($num)
 {
     if ($num % 2 == 0) {
         return "par";
     } else {
         echo "impar";
     };
 };



 echo par_ou_impar(4);
 echo "<hr>";



// Function Bidimensional
 function par_ou_imparV2($numero)
 {
     return ($numero % 2 == 0)? "Número Par" : "Número Impar";
 }

 echo par_ou_imparV2(5);
 echo "<hr>";


 
 
// Contador de senhas FOR
 function gerador_senha_ComFOR ($senhaInicial, $senhaFinal)
 {
    for($contador = $senhaInicial; $contador <= $senhaFinal; $contador++){
        echo $contador. '-';
    };

 };

 gerador_senha_ComFOR(10,20);   //não é necessário o echo aqui, pois já está escrito na função
 echo "<hr>";




#Contador de senhas WHILE
 function gerador_senha_ComWHILE ($senhaInicial, $senhaFinal )
 {
    $contador = $senhaInicial;

    while($contador <= $senhaFinal){
        echo $contador.'-';
        $contador++;
    }

 };

 gerador_senha_ComWHILE(6,30); 
 echo "<hr>";      